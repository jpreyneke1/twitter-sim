# Twitter-sim

## Name

Twitter-like feed simulation

## Description

This project simulates a Twitter-like feed, enabling users to follow each other and share tweets. The program takes input from two files: one containing user information, including their followers, and the other containing the tweets posted by the users.

## Installation

To install the project, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/jpreyneke1/twitter-sim.git
   ```

2. Enter the project directory:

   ```bash
   cd twitter-sim
   ```

3. Ensure you have Go installed:

   ```bash
   go version
   ```

4. Install project dependencies:

   ```bash
   go mod download
   ```

## Usage

### Run Locally

1. Make sure you are in the project cmd directory:

   ```bash
   cd twitter-sim/cmd
   ```

2. Run the program using the following command:

   Default:

   ```bash
   go run main.go
   ```

   OR (with flags)

   ```bash
   go run main.go -usersFile="../upload-files/user.txt" -tweetsFile="../upload-files/tweet.txt" -logLevel="error"
   ```

   Replace `../upload-files/user.txt` with the path to your user file and `../upload-files/tweet.txt` with the path to your tweets file.

   Optional: Adjust the log level (logLevel) according to your preference (options: debug, info, warn, error, fatal, panic).

   Note: If no flags are provided, the program will load example files from the upload-files directory by default

### Run in Docker

1. Make sure you are in the project root directory:

   ```bash
   cd twitter-sim
   ```

2. Build the Docker image:

   ```bash
   docker build -t twitter-sim .
   ```

3. Run the Docker Container (no flags provided it will load the examples files from the upload-files directory)

   ```bash
   docker run twitter-sim
   ```

## Support

For support or questions, please email me at jpreyneke1@gmail.com

## Roadmap

Nothing planned at the moment.

## Project Assumptions
- Usernames must be unique; multiple users with the same name are not allowed.
- Tweets are prohibited from containing the character '>'.
- Empty lines will generate a warning, but the execution will proceed.
- There are no limitations on the number of users, follows, or tweets.
- File sizes are not restricted.