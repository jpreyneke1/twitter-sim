package main

import (
	"flag"
	"fmt"
	"twitter_sim/internal/processor"
	"twitter_sim/internal/util"

	"github.com/sirupsen/logrus"
)

func main() {

	logrus.SetFormatter(&logrus.TextFormatter{})
	usersFile := flag.String("usersFile", "../upload-files/user.txt", "Path to the users file")
	tweetsFile := flag.String("tweetsFile", "../upload-files/tweet.txt", "Path to the tweets file")
	logLevel := flag.String("logLevel", "error", "Log level (options: debug, info, warn, error, fatal, panic)")

	// parse command-line arguments
	flag.Parse()

	// configure logging level
	level, err := logrus.ParseLevel(*logLevel)
	if err != nil {
		logrus.Fatal("Invalid log level:", *logLevel)
	}
	logrus.SetLevel(level)

	logrus.Info("Twitter-sim Started")

	// check if files exist
	if !util.FileExists(*usersFile) {
		logrus.WithField("file", *usersFile).Fatal("Users file does not exist")
	}
	if !util.FileExists(*tweetsFile) {
		logrus.WithField("file", *tweetsFile).Fatal("Tweets file does not exist")
	}

	// retrieve content from files
	usersContent, tweetsContent, fileReadErr := util.ReadFilesConcurrently(*usersFile, *tweetsFile)
	if fileReadErr != nil {
		logrus.Fatal(fileReadErr)
	}

	// process user file
	users, err := processor.ProcessUserFile(string(usersContent))
	if err != nil {
		logrus.WithField("processor", "ProcessUserFile").Fatal(err)
	}

	// process tweet file
	err = processor.ProcessTweetFile(string(tweetsContent), users)
	if err != nil {
		logrus.WithField("processor", "ProcessTweetFile").Fatal(err)
	}

	// print feed
	output := processor.GenerateUserFeed(users)
	fmt.Println(output)

	logrus.Info("Twitter-sim Completed")

}
