package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(t *testing.T) {
	// create temp dir
	tmpDir, err := os.MkdirTemp("", "twitter_sim_test")
	assert.NoError(t, err)
	defer os.RemoveAll(tmpDir)

	usersFile := fmt.Sprintf("%s/users.txt", tmpDir)
	tweetsFile := fmt.Sprintf("%s/tweets.txt", tmpDir)

	// write sample test data
	err = os.WriteFile(usersFile, []byte("John follows Kerry\n"), 0644)
	assert.NoError(t, err)
	err = os.WriteFile(tweetsFile, []byte("Kerry> Hello, World!\n"), 0644)
	assert.NoError(t, err)

	os.Args = []string{"cmd", "-usersFile=" + usersFile, "-tweetsFile=" + tweetsFile, "-logLevel=error"}
	assert.NotPanics(t, func() { main() }, "main function should not panic")
}
