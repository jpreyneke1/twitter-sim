package model

import (
	"errors"
)

// use to track order of tweets
var TWEET_ID_COUNTER int
var TWEET_MAX_LENTH = 140

type Tweet struct {
	ID      int
	Message string
	User    *User
}

func NewTweet(message string, user *User) (Tweet, error) {
	if err := ValidateMessage(message); err != nil {
		return Tweet{}, err
	}

	TWEET_ID_COUNTER++
	return Tweet{
		ID:      TWEET_ID_COUNTER,
		Message: message,
		User:    user,
	}, nil
}

func ValidateMessage(message string) error {

	if len(message) > TWEET_MAX_LENTH {
		return errors.New("Tweet message exceeds maximum length of 140 characters")
	}

	return nil
}
