package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddFollow(t *testing.T) {
	userJohn := NewUser("John")
	userKerry := NewUser("Kerry")
	userJohn.AddFollow(userKerry)

	assert.Contains(t, userJohn.Follows, userKerry, "Expected John to follow Kerry")
	assert.NotContains(t, userKerry.Follows, userJohn, "Expected Kerry not to follow John")
}

func TestAddTweet(t *testing.T) {
	user := NewUser("John")
	tweet := &Tweet{ID: 1, Message: "A test tweet"}
	user.AddTweet(tweet)

	assert.Len(t, user.Tweets, 1, "John should have 1 tweet")
	assert.Equal(t, tweet, user.Tweets[0], "Expects tweets created to match")
}

func TestGetFollowedUserTweets(t *testing.T) {
	userJohn := NewUser("John")
	userKerry := NewUser("Kerry")
	userAlice := NewUser("Alice")

	tweetJohn1 := &Tweet{ID: 1, Message: "John tweet 1"}
	tweetJohn2 := &Tweet{ID: 2, Message: "John tweet 2"}

	tweetKerry1 := &Tweet{ID: 3, Message: "Kerry tweet 1"}
	tweetKerry2 := &Tweet{ID: 4, Message: "Kerry tweet 2"}

	userJohn.AddTweet(tweetJohn1)
	userJohn.AddTweet(tweetJohn2)

	userKerry.AddTweet(tweetKerry1)
	userKerry.AddTweet(tweetKerry2)

	userAlice.AddFollow(userJohn)
	userAlice.AddFollow(userKerry)

	// follow new users
	followedTweets := userAlice.GetFollowedUserTweets()

	expectedTweets := []*Tweet{tweetJohn1, tweetJohn2, tweetKerry1, tweetKerry2}
	assert.Equal(t, expectedTweets, followedTweets, "Expected tweets should match followed tweets")
}
