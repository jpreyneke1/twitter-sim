package model

var USER_ID_COUNTER int

type User struct {
	ID      int
	Name    string
	Follows map[*User]struct{}
	Tweets  []*Tweet
}

func NewUser(name string) *User {
	USER_ID_COUNTER++
	return &User{
		ID:      USER_ID_COUNTER,
		Name:    name,
		Follows: make(map[*User]struct{}),
		Tweets:  make([]*Tweet, 0),
	}
}

func (u *User) AddFollow(follow *User) {
	u.Follows[follow] = struct{}{}
}

func (u *User) AddTweet(tweet *Tweet) {
	u.Tweets = append(u.Tweets, tweet)
}

func (u *User) GetFollowedUserTweets() []*Tweet {
	var followedTweets []*Tweet

	for followedUser := range u.Follows {
		followedUserTweets := followedUser.Tweets

		followedTweets = append(followedTweets, followedUserTweets...)
	}

	return followedTweets
}
