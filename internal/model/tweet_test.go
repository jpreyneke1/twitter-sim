package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewTweet(t *testing.T) {
	userJohn := NewUser("John")
	message := "A test tweet"

	tweet, err := NewTweet(message, userJohn)

	assert.NoError(t, err, "Error should be nil")
	assert.NotNil(t, tweet, "Tweet should not be nil")
	assert.Equal(t, message, tweet.Message, "Message should match")
}

func TestValidateMessage(t *testing.T) {

	testCases := []struct {
		name        string
		message     string
		expectedErr bool
	}{
		{
			name:        "ValidTweetMessage",
			message:     "A short story",
			expectedErr: false,
		},
		{
			name:        "InvalidTweetMessage",
			message:     "This message is meant to cause trouble. It is intentionally created to exceed the allowed character limit for testing our validation. We want to make sure it works properly so we have made this message longer than allowed.",
			expectedErr: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := ValidateMessage(tc.message)
			if tc.expectedErr {
				assert.Error(t, err, "Expected an error: %s", tc.message)
			} else {
				assert.NoError(t, err, "Expected no error: %s", tc.message)
			}
		})
	}
}
