package util

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFileExists(t *testing.T) {
	// create a temporary file for testing
	tempFile, err := os.CreateTemp("", "tempfile")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tempFile.Name())

	// test existing file
	assert.True(t, FileExists(tempFile.Name()), "Expected file to exist, but it does not.")

	// test non-existing file
	assert.False(t, FileExists("nonexistentfile"), "Expected the file to be non-existent, but it was found.")
}

func TestReadFilesConcurrently(t *testing.T) {
	// create test files
	usersFile, err := os.CreateTemp("", "usersfile")
	assert.NoError(t, err)
	defer os.Remove(usersFile.Name())

	tweetsFile, err := os.CreateTemp("", "tweetsfile")
	assert.NoError(t, err)
	defer os.Remove(tweetsFile.Name())

	// write content to test files
	usersContent := []byte("Users file content")
	tweetsContent := []byte("Tweets file content")
	err = os.WriteFile(usersFile.Name(), usersContent, 0644)
	assert.NoError(t, err)
	err = os.WriteFile(tweetsFile.Name(), tweetsContent, 0644)
	assert.NoError(t, err)

	returnedUserContent, returnedTweetsContent, errors := ReadFilesConcurrently(usersFile.Name(), tweetsFile.Name())

	// compare content
	assert.Equal(t, returnedUserContent, string(usersContent))
	assert.Equal(t, returnedTweetsContent, string(tweetsContent))

	// check errors
	assert.Empty(t, errors, "Expected no errors, but got errors.")
}

func TestReadFilesConcurrently_Error(t *testing.T) {
	nonExistentFile := "nonexistentfile.txt"
	content1, content2, errors := ReadFilesConcurrently(nonExistentFile, nonExistentFile)

	// check content
	assert.Empty(t, content1, "Expected content1 to be empty.")
	assert.Empty(t, content2, "Expected content2 to be empty.")

	// check errors
	assert.Len(t, errors, 2, "Expected 2 errors, but got %d errors.")

	for _, err := range errors {
		assert.NotNil(t, err.Err, "Expected non-nil error, but got nil error.")
	}
}

func TestIsSevenBitASCII(t *testing.T) {
	// seven-bit ASCII content
	asciiContent := []byte("This is seven-bit ASCII!")
	isASCII := isSevenBitASCII(asciiContent)
	assert.True(t, isASCII, "Expected content to be seven-bit ASCII.")

	// non-seven-bit ASCII content
	nonASCIIContent := []byte("This contains non-ASCII character: é")
	isASCII = isSevenBitASCII(nonASCIIContent)
	assert.False(t, isASCII, "Expected content not to be seven-bit ASCII.")
}
