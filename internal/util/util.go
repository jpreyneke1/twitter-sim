package util

import (
	"errors"
	"os"
	"sync"

	"github.com/sirupsen/logrus"
)

type ReadFileError struct {
	Filename string
	Err      error
}

func FileExists(filepath string) bool {
	_, err := os.Stat(filepath)
	return !os.IsNotExist(err)
}

func ReadFilesConcurrently(usersFile, tweetsFile string) (string, string, []ReadFileError) {
	var wg sync.WaitGroup
	usersResultChan := make(chan string, 1)
	tweetsResultChan := make(chan string, 1)
	errChan := make(chan ReadFileError, 2)

	wg.Add(2)
	go readFile(usersFile, usersResultChan, errChan, &wg)
	go readFile(tweetsFile, tweetsResultChan, errChan, &wg)

	wg.Wait()

	// close channels after goroutines finish
	close(usersResultChan)
	close(tweetsResultChan)
	close(errChan)

	// collect errors
	var errors []ReadFileError
	for err := range errChan {
		errors = append(errors, err)
	}

	logrus.Info("Finished reading files concurrently")

	// extract results
	usersContent := <-usersResultChan
	tweetsContent := <-tweetsResultChan

	return usersContent, tweetsContent, errors
}

func readFile(filename string, resultChan chan<- string, errChan chan<- ReadFileError, wg *sync.WaitGroup) {
	defer wg.Done()

	logrus.WithFields(logrus.Fields{"filename": filename}).Info("Reading file")

	content, err := os.ReadFile(filename)
	if err != nil {
		readFileErr := ReadFileError{Filename: filename, Err: err}
		errChan <- readFileErr

		logrus.WithFields(logrus.Fields{"filename": filename}).Errorf("Error reading file: %v", err)
		return
	}

	// check if the content is seven-bit ASCII
	if !isSevenBitASCII(content) {
		readFileErr := ReadFileError{Filename: filename, Err: errors.New("File is not seven-bit ASCII")}
		errChan <- readFileErr

		logrus.WithFields(logrus.Fields{"filename": filename}).Error("File is not seven-bit ASCII")
		return
	}

	resultChan <- string(content)
	logrus.WithFields(logrus.Fields{"filename": filename}).Info("Finished reading file")
}

func isSevenBitASCII(content []byte) bool {
	for _, b := range content {
		if b > 127 {
			return false
		}
	}
	return true
}
