package processor

import (
	"errors"
	"testing"
	"twitter_sim/internal/model"

	"github.com/stretchr/testify/assert"
)

func TestProcessTweetFile(t *testing.T) {
	testCases := []struct {
		name     string
		content  string
		users    map[string]*model.User
		expected error
	}{
		{
			name:     "Valid Tweet",
			content:  "Alan> If you have a procedure with 10 parameters, you probably missed some.",
			users:    map[string]*model.User{"Alan": model.NewUser("Alan")},
			expected: nil,
		},
		{
			name:     "Invalid Tweet",
			content:  "InvalidTweet",
			users:    map[string]*model.User{"Alan": model.NewUser("Alan")},
			expected: errors.New("Invalid line detected on tweet file"),
		},
		{
			name:     "User Not Found",
			content:  "NonExistentUser> This should fail",
			users:    map[string]*model.User{"Alan": model.NewUser("Alan")},
			expected: errors.New("User not found for tweet"),
		},
		{
			name:     "Validation Error",
			content:  "Alan> This message is meant to cause trouble. It is intentionally created to exceed the allowed character limit for testing our validation. We want to make sure it works properly so we have made this message longer than allowed.",
			users:    map[string]*model.User{"Alan": model.NewUser("Alan")},
			expected: errors.New("Tweet message exceeds maximum length of 140 characters"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := ProcessTweetFile(tc.content, tc.users)
			assert.Equal(t, tc.expected, err, "Unexpected error for test case: "+tc.name)
		})
	}
}
