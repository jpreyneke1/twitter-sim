package processor

import (
	"errors"
	"fmt"
	"sort"
	"strings"
	"twitter_sim/internal/model"

	"github.com/sirupsen/logrus"
)

func ProcessUserFile(content string) (map[string]*model.User, error) {
	logrus.Info("ProcessUserFile Started")

	users := make(map[string]*model.User)

	lines := strings.Split(content, "\n")

	for _, line := range lines {

		// sanitize
		line = strings.TrimSpace(line)
		if line == "" {
			logrus.Warn("Empty line detected on file, skipped")
			continue // skip empty lines
		}

		// split into words
		words := strings.Fields(line)

		// validate words
		if len(words) < 3 || words[1] != "follows" {
			err := errors.New("Invalid line detected on user file")
			logrus.WithFields(logrus.Fields{"line": line}).Error(err)
			return nil, err
		}

		username := words[0]

		// split into follows
		follows := strings.Split(strings.Join(words[2:], ""), ",")

		// check if user exist on map
		user, exist := users[username]
		if !exist {
			user = model.NewUser(username)
			users[username] = user
		}

		for _, followedUsername := range follows {

			// sanitize
			followedUsername = strings.TrimSpace(followedUsername)

			// check if user exist
			followedUser, exist := users[followedUsername]
			if !exist {
				followedUser = model.NewUser(followedUsername)
				users[followedUsername] = followedUser
			}
			// add as follower to user
			user.AddFollow(followedUser)
		}

	}
	logrus.Info("ProcessUserFile Completed")
	return users, nil
}

func GenerateUserFeed(users map[string]*model.User) string {
	var userList []model.User
	var output strings.Builder

	// create list to sort
	for _, user := range users {
		userList = append(userList, *user)
	}

	// sort users alphabetically
	sort.Slice(userList, func(i, j int) bool {
		return userList[i].Name < userList[j].Name
	})

	// print feed
	for _, user := range userList {
		output.WriteString(user.Name + "\n")

		// retrieve all tweets for feed
		allTweets := append(user.Tweets, user.GetFollowedUserTweets()...)

		// sort tweets by created id
		sort.Slice(allTweets, func(i, j int) bool {
			return allTweets[i].ID < allTweets[j].ID
		})

		for _, tweet := range allTweets {
			fmt.Fprintf(&output, "\t@%s: %s\n", tweet.User.Name, tweet.Message)
		}
	}

	// sanitize
	result := strings.TrimSpace(output.String())

	return result
}
