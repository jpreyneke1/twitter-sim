package processor

import (
	"errors"
	"strings"
	"twitter_sim/internal/model"

	"github.com/sirupsen/logrus"
)

func ProcessTweetFile(content string, users map[string]*model.User) error {
	logrus.Info("ProcessTweetFile Started")

	lines := strings.Split(content, "\n")

	for _, line := range lines {

		// sanitize
		line = strings.TrimSpace(line)
		if line == "" {
			logrus.Warn("Empty line detected on tweet file, skipped")
			continue // skip empty lines
		}

		// split into array of 2
		splitResult := strings.Split(line, "> ")

		// should only have 2 elements
		if len(splitResult) != 2 {
			err := errors.New("Invalid line detected on tweet file")
			logrus.WithFields(logrus.Fields{"line": line}).Error(err)
			return err
		}

		username := splitResult[0]
		tweetMsg := splitResult[1]

		// check if user exists
		user, exist := users[username]
		if !exist {
			err := errors.New("User not found for tweet")
			logrus.WithFields(logrus.Fields{"line": line}).Error(err)
			return err
		}

		// create a tweet
		tweet, err := model.NewTweet(tweetMsg, user)
		if err != nil {
			logrus.WithFields(logrus.Fields{"tweet": tweetMsg, "username": user.Name}).Error(err)
			return err
		}

		// add new tweet to user
		user.AddTweet(&tweet)
	}

	logrus.Info("ProcessTweetFile Completed")
	return nil
}
