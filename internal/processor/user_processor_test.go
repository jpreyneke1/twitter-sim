package processor

import (
	"testing"
	"twitter_sim/internal/model"

	"github.com/stretchr/testify/assert"
)

func TestProcessUserFile(t *testing.T) {
	userJohn := model.NewUser("John")
	userKerry := model.NewUser("Kerry")
	userJohn.AddFollow(userKerry)

	testCases := []struct {
		name      string
		content   string
		expected  map[string]*model.User
		expectErr bool
	}{
		{
			name:      "Valid User File",
			content:   "John follows Kerry",
			expected:  map[string]*model.User{"John": userJohn, "Kerry": userKerry},
			expectErr: false,
		},
		{
			name:      "Invalid User File",
			content:   "InvalidUserFileContent",
			expected:  nil,
			expectErr: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			users, err := ProcessUserFile(tc.content)

			if tc.expectErr {
				assert.Error(t, err, "Expected error for invalid user file")
			} else {
				assert.NoError(t, err, "Expected no error for valid user file")

				for _, user := range users {
					expectedUser, ok := tc.expected[user.Name]
					assert.True(t, ok, "Expected user not found in the map")
					assert.Equal(t, expectedUser.Name, user.Name, "User names should match")
					assert.Len(t, expectedUser.Follows, len(user.Follows), "Number of follows should match")
				}
			}
		})
	}
}

func TestPrintUserTweets(t *testing.T) {

	// create users
	userAlan := model.NewUser("Alan")
	userMartin := model.NewUser("Martin")
	userWard := model.NewUser("Ward")

	tweetAlan1 := &model.Tweet{User: userAlan, Message: "If you have a procedure with 10 parameters, you probably missed some.", ID: 1}
	tweetWard1 := &model.Tweet{User: userWard, Message: "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.", ID: 2}
	tweetAlan2 := &model.Tweet{User: userAlan, Message: "Random numbers should not be generated with a method chosen at random.", ID: 3}

	userAlan.AddTweet(tweetAlan1)
	userWard.AddTweet(tweetWard1)
	userAlan.AddTweet(tweetAlan2)

	userAlan.AddFollow(userMartin)
	userWard.AddFollow(userAlan)
	userWard.AddFollow(userMartin)

	// valid case
	users := make(map[string]*model.User)
	users["Alan"] = userAlan
	users["Martin"] = userMartin
	users["Ward"] = userWard

	actualOutput := GenerateUserFeed(users)

	// compare print
	expectedOutput := "Alan\n\t@Alan: If you have a procedure with 10 parameters, you probably missed some.\n\t@Alan: Random numbers should not be generated with a method chosen at random.\nMartin\nWard\n\t@Alan: If you have a procedure with 10 parameters, you probably missed some.\n\t@Ward: There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.\n\t@Alan: Random numbers should not be generated with a method chosen at random."
	assert.Equal(t, expectedOutput, actualOutput, "Printed user tweets should match expected output")

}
