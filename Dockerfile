FROM golang:1.21.4-alpine
WORKDIR /app
COPY . .
WORKDIR /app/cmd
CMD ["go", "run", "main.go"]
